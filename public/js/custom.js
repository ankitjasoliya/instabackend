$(document).ready(function() {

    $('#insta-slider').owlCarousel({
        loop: true,
        margin: 0,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            480: {
                items: 2,
                nav: false
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 4,
                nav: true,
                loop: false,
                margin: 20
            }
        }
    })
})
$(document).ready(function() {
    $('.dm-next_btn').click(function() {
        var self = $(this);
        $(this).each(function() {
            $('.dm-popup-lightbox').css('display', 'none');
        });
        self.parents('.dm-popup-lightbox').next().css('display', 'block');
    });
    $('.dm-pre_btn').click(function() {
        var selfs = $(this);
        $(this).each(function() {
            $('.dm-popup-lightbox').css('display', 'none');
        });
        selfs.parents('.dm-popup-lightbox').prev().css('display', 'block');
    });
});