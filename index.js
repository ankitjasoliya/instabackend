const express = require('express');

var path = require('path');
const router = express.Router();

var bodyParser = require("body-parser");
const cors = require('cors');
const mongoose = require('mongoose');
const request = require('request-promise');

const Shopify = require('shopify-api-node');


require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

let Facebook = require('./models/facebook.model');
let Facebookpage = require('./models/facebookpage.model');
let Shop = require('./models/shop.model');
let Instagram = require('./models/instagram.model');
let Settings = require('./models/settings.model');

app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));


const uri = process.env.ATLAS_URI;
mongoose.Promise = global.Promise;

mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true}
);
const connection = mongoose.connection;
connection.once('open', () => {
    console.log("MongoDB database connection established successfully");
})

// set the view engine to ejs
app.set('view engine', 'ejs');


app.get('/', function(req, res) {

    var shop_id = 'lasy-international.myshopify.com';

    Shop.findOne({shop: shop_id}, function (err, shopdata) {
        Instagram.findOne({shop:shop_id},function (err,instadata) {
            console.log(instadata);
            if(!err && instadata!=null){
                if(instadata.page_id!='' && instadata.insta_id!=''){
                    Facebookpage.findOne({shop:shop_id,page_id:instadata.page_id},function (err,pagedata) {
                        if(pagedata!=null){
                            const InstagramDataURL = "https://graph.facebook.com/"+ instadata.insta_id +"?fields=profile_picture_url&access_token="+ pagedata.page_accesss_token;

                            request.get(InstagramDataURL)
                                .then((Instaprresponse) => {
                                var profilrdata = JSON.parse(Instaprresponse);

                            const InstagramURL = "https://graph.facebook.com/"+ instadata.insta_id +"/media?fields=media_url,like_count,timestamp,comments_count,text,user,username,caption,permalink&access_token="+ pagedata.page_accesss_token;

                            request.get(InstagramURL)
                                .then((Instaresponse) => {
                                var allpostdata = JSON.parse(Instaresponse);

                                var newdataarray = new Array();
                                allpostdata.data.forEach(function (item,value) {
                                    var d = new Date(item.timestamp);
                                    var month = d.getMonth();
                                    var newdate = d.getDate();
                                    const monthNames = ["January", "February", "March", "April", "May", "June",
                                        "July", "August", "September", "October", "November", "December"
                                    ];
                                    var finaldatename = monthNames[month]+' '+ newdate;

                                    var newarray = {
                                        'profile' : profilrdata.profile_picture_url,
                                        'text' : item.text,
                                        'media_url' : item.media_url,
                                        'like_count' : item.like_count,
                                        'timestamp' : finaldatename,
                                        'comments': item.comments_count,
                                        'username': item.username,
                                        'caption': item.caption,
                                        'instalink': item.permalink
                                    }
                                    newdataarray.push(newarray);
                                });

                                res.render('pages/index',{allpost:newdataarray});

                        }).catch((error1) => {
                                res.send(false);
                            // console.log(error1);
                        });
                        }).catch((error1) => {
                                res.send(false);
                            // console.log(error1);
                        });
                       }
                    });


                }

            }

        })
    });



});


app.post('/save_shop', function (req, res) {

    var shop = req.body.shop;
    var access_token = req.body.accessToken;

    const SHOPIFYAPIDATA = new Shopify({
      shopName: shop,
      accessToken: access_token
    });

    SHOPIFYAPIDATA.scriptTag.create({
        event:'onload',
        src:'http://localhost:5000/js/instagrammagic.js'
    }).then(
    (scriptTag) => console.log(scriptTag),
    (err) => console.error(err)
  );

    Shop.findOne({shop: shop}, function (err, data) {

        if(data==null){

            const newShop = new Shop({
                shop,
                access_token
            });

            const shopify = new Shopify({
                shopName: shop,
                accessToken: access_token
            });

            shopify.shop
                .get()
                .then((data) => console.log(data))
        .catch((err) => console.error(err));


            newShop.save(function (error) {
                if (error) {
                    res.send('false');
                    console.error(error);
                }else{
                    console.log("Shop Has Been Added");
                    res.send('true');
                }
            });
        }else{

            console.log(shop);
            console.log(access_token);

            data.shop  = shop;
            data.access_token = access_token
            data.save();




            //     const accessTokenRequestUrl1 = 'https://'+shop + '/admin/api/2020-04/shop.json';
            //
            //     const accessTokenPayload = {
            //         shop: shop, // MYSHOP.myshopify.com
            //         shopify_api_key: process.env.SHOPIFY_API_KEY, // Your API key
            //         shopify_shared_secret: process.env.SHOPIFY_API_SECRET, // Your Shared Secret
            //         access_token: access_token
            //     };
            //
            //
            //     request.get(accessTokenRequestUrl1,{json:accessTokenPayload})
            //         .then((accessTokenResponse1) => {
            //         console.log(accessTokenResponse1);
            //         // result = JSON.parse(accessTokenResponse1);
            //
            //
            //     // if(result.data!='undefined'){
            //     //     res.send(accessTokenResponse1);
            //     // }else{
            //     //     res.send('No Pages Right Now')
            //     // }
            // }).catch((error) => {
            //         console.log(error);
            //     // res.status(error.statusCode).send(error.error.error_description);
            // });


            res.send('Shop Updated');
            console.log('21222')
        }
    });



});

app.post('/check_login_account', function (req, res) {

    const shop = req.body.shop;

    Shop.findOne({shop: shop}, function (err, data) {
        console.log(data);
        if(!err){
            const apiRequestUrl = 'https://' +shop+ '/admin/shop.json';
            const apiRequestHeader = {
                'X-Shopify-Access-Token':data.access_token
            };

            request.get(apiRequestUrl,{headers:apiRequestHeader })
                .then((apiResponse) => {

            var result = JSON.parse(apiResponse);

            data.name = result.shop.name;
            data.email = result.shop.email;
            data.country = result.shop.country;
            data.address1 = result.shop.address1;
            data.created_at = result.shop.created_at;
            data.country_name = result.shop.country_name;
            data.shop_owner = result.shop.shop_owner;
            data.customer_email = result.shop.customer_email;
            data.plan_name = result.shop.plan_name;
            data.save();

        })
        .catch((error)=> {

            res.status(error.statusCode).send(error);
        });

            Facebook.findOne({shop: shop,main_id:data._id}, function (err, facebookdata) {
                if(!err && facebookdata!=null){
                    res.send('true');
                }else{
                    res.send('false');
                }


            });


        }



    });




});

app.post('/get_fb_pages', function (req, res) {
    const shop_id = req.body.shop;

    Shop.findOne({shop: shop_id}, function (err, data1) {
        var main_id = data1._id;
        var shop = data1.shop;

        Facebook.findOne({shop: shop, main_id:main_id}, function (err, data) {

            var fb_id = data.userID;
            var access_token = data.accesss_token;

            if(fb_id!=null && access_token!=null){
                const accessTokenRequestUrl1 = 'https://graph.facebook.com/'+fb_id+'/accounts?access_token='+access_token+'';

                request.get(accessTokenRequestUrl1)
                    .then((accessTokenResponse1) => {

                    result = JSON.parse(accessTokenResponse1);

                if(result.data!='undefined'){

                    result.data.forEach(function (item,index) {
                        Facebookpage.findOne({ page_id : item.id, fb_id:fb_id },function (err, pagedata) {
                            if(!err && pagedata!=null){
                                pagedata.main_id = main_id;
                                pagedata.shop = shop_id;
                                pagedata.page_name = item.name;
                                pagedata.page_category = item.category;
                                pagedata.page_accesss_token = item.access_token;
                                pagedata.save();
                            }else{

                                var page_id = item.id;
                                var page_name = item.name;
                                var page_category = item.category;
                                var page_accesss_token = item.access_token;

                                const newFacebookpage = new Facebookpage({
                                    main_id,
                                    shop,
                                    fb_id,
                                    page_id,
                                    page_name,
                                    page_category,
                                    page_accesss_token
                                });

                                newFacebookpage.save(function (error) {
                                    console.log("Your Page has been saved!");
                                    if (error) {
                                        console.error(error);
                                    }
                                });

                            }
                        })
                    })

                    res.send(accessTokenResponse1);
                }else{
                    res.send('No Pages Right Now')
                }
            }).catch((error) => {
                    console.log(error);
                // res.status(error.statusCode).send(error.error.error_description);
            });
            }else{
                res.send('Facebook Account Not Connected')
            }


        });
    });

});
app.post('/get_settings', function (req, res) {
    var shop_id = req.body.shop;
    // console.log(shop_id);

    Shop.findOne({shop: shop_id}, function (err, data1) {
        var main_id = data1._id;
        var shop = data1.shop;

        Settings.findOne({shop: shop, main_id:main_id}, function (err, getdata) {
            // console.log(getdata);
            if(!err && getdata!=null){
                res.send(getdata);
            }else{
                res.send(false);
            }
        });

    });


});

app.post('/save_settings', function (req, res) {
    var shop_id = req.body.shop;
    var postdata = req.body.settings;
    var option_type = req.body.option;

    Shop.findOne({shop: shop_id}, function (err, data1) {
        var main_id = data1._id;
        var shop = data1.shop;

        if(option_type==1){

            var popover_active = postdata.popoverActive;
            var check_widget = postdata.checkwidget;
            var check_hover = postdata.checkhover;
            var display_border = postdata.display_border;
            var social_sharing = postdata.social_sharing;
            var border_width_value = postdata.border_width_value;
            var widget_heading_value = postdata.widget_heading_value;
            var layout_column_value = postdata.layout_column_value;
            var layout_row_value = postdata.layout_row_value;
            var padding_value = postdata.padding_value;
            var color = JSON.stringify(postdata.color);

        }else if(option_type==2){

            var info_like = postdata.info_like;
            var info_comment = postdata.info_comment;
            var info_description = postdata.info_description;
            var popup_like = postdata.popup_like;
            var popup_comment = postdata.popup_comment;
            var popup_description = postdata.popup_description;
        }else if(option_type==3){

            var widgetcolor = JSON.stringify(postdata.widgetcolor);
            var overlaycolor = JSON.stringify(postdata.overlaycolor);
            var textcolor = JSON.stringify(postdata.textcolor);
            var pbackgroundcolor = JSON.stringify(postdata.pbackgroundcolor);
            var ptextcolor = JSON.stringify(postdata.ptextcolor);
        }else if(option_type==4){

            var shopbackgroundcolor = JSON.stringify(postdata.shopbackgroundcolor);
            var shoptextcolor = JSON.stringify(postdata.shoptextcolor);
            var shop_heading_value = postdata.shop_heading_value;
            var shop_button = postdata.shop_button;

        }


        Settings.findOne({shop: shop, main_id:main_id}, function (err, setdata) {

            if(!err && setdata!=null){
                setdata.main_id = main_id;
                setdata.shop = shop_id;
                if(option_type==1){

                    setdata.popover_active = popover_active;
                    setdata.check_widget = check_widget;
                    setdata.check_hover = check_hover;
                    setdata.display_border = display_border;
                    setdata.social_sharing = social_sharing;
                    setdata.border_width_value = border_width_value;
                    setdata.widget_heading_value = widget_heading_value;
                    setdata.layout_column_value = layout_column_value;
                    setdata.layout_row_value = layout_row_value;
                    setdata.padding_value = padding_value;
                    setdata.color = color;
                }else if(option_type==2) {

                    setdata.info_like = info_like;
                    setdata.info_comment = info_comment;
                    setdata.info_description = info_description;
                    setdata.popup_like = popup_like;
                    setdata.popup_comment = popup_comment;
                    setdata.popup_description = popup_description;
                }else if(option_type==3) {

                    setdata.widgetcolor = widgetcolor;
                    setdata.overlaycolor = overlaycolor;
                    setdata.textcolor = textcolor;
                    setdata.pbackgroundcolor = pbackgroundcolor;
                    setdata.ptextcolor = ptextcolor;

                }else if(option_type==4) {

                    setdata.shopbackgroundcolor = shopbackgroundcolor;
                    setdata.shoptextcolor = shoptextcolor;
                    setdata.shop_heading_value = shop_heading_value;
                    setdata.shop_button = shop_button;
                }

                setdata.save();
            }else{
                if(option_type==1){
                    const newSettingspage = new Settings({
                        main_id,
                        shop,
                        popover_active,
                        check_widget,
                        check_hover,
                        display_border,
                        social_sharing,
                        border_width_value,
                        widget_heading_value,
                        layout_column_value,
                        layout_row_value,
                        padding_value,
                        color
                    });
                    newSettingspage.save(function (error) {
                        console.log("Your Settings has been saved!");
                        if (error) {
                            console.error(error);
                        }
                    });

                }else if(option_type==2) {
                    const newSettingspage = new Settings({
                        main_id,
                        shop,
                        info_like,
                        info_comment,
                        info_description,
                        popup_like,
                        popup_comment,
                        popup_description
                    });

                    newSettingspage.save(function (error) {
                        console.log("Your Settings has been saved!");
                        if (error) {
                            console.error(error);
                        }
                    });

                }else if(option_type==3) {
                    const newSettingspage = new Settings({
                        main_id,
                        shop,
                        widgetcolor,
                        overlaycolor,
                        textcolor,
                        pbackgroundcolor,
                        ptextcolor
                    });

                    newSettingspage.save(function (error) {
                        console.log("Your Settings has been saved!");
                        if (error) {
                            console.error(error);
                        }
                    });

                }else if(option_type==4) {
                    const newSettingspage = new Settings({
                        main_id,
                        shop,
                        shopbackgroundcolor,
                        shoptextcolor,
                        shop_heading_value,
                        shop_button
                    });

                    newSettingspage.save(function (error) {
                        console.log("Your Settings has been saved!");
                        if (error) {
                            console.error(error);
                        }
                    });

                }


            }


            res.send(true);

        });


    });

});


app.post('/save_accesstoken', function (req, res) {


    const shop_id = req.body.shop;
    const name = req.body.response.name;
    const email = req.body.response.email;
    const picture = req.body.response.picture.data.url;
    const userID = req.body.response.userID;


    const accessTokenRequestUrl = 'https://graph.facebook.com/v4.0/oauth/access_token';
    const accessTokenPayload = {
        client_id:process.env.FACEBOOK_CLIENT_ID,
        client_secret:process.env.FACEBOOK_CLIENT_SECRET,
        grant_type : 'fb_exchange_token',
        fb_exchange_token : req.body.response.accessToken
    };

    console.log(accessTokenPayload);
    // https://graph.facebook.com/{graph-api-version}/oauth/access_token?       grant_type=fb_exchange_token&               client_id={app-id}&     client_secret={app-secret}&     fb_exchange_token={your-access-token}

    request.post(accessTokenRequestUrl,{json:accessTokenPayload})
        .then((accessTokenResponse) => {

        const accesss_token = accessTokenResponse.access_token;

    Shop.findOne({shop: shop_id}, function (err, data1) {
        // console.log(data1);
        var main_id = data1._id;
        var shop = data1.shop;

        Facebook.findOne({shop: shop, main_id:main_id}, function (err, data) {
            if(data==null){
                const newFacebook = new Facebook({
                    main_id,
                    shop,
                    name,
                    email,
                    picture,
                    userID,
                    accesss_token
                });

                newFacebook.save(function (error) {
                    console.log("Your bee has been saved!");
                    if (error) {
                        console.error(error);
                    }
                });
            }else{

                data.main_id = main_id;
                data.shop = shop;
                data.name = name;
                data.email = email;
                data.picture = picture;
                data.userID = userID;
                data.accesss_token = accesss_token;
                data.save();

                console.log('21222')
            }

            res.send('true');

        });
    });




}).catch((error) => {
        console.log(error);
    // res.status(error.statusCode).send(error.error.error_description);
});

});
app.post('/get_insta_post', function (req, res) {
    var shop_id = req.body.shop;

    Shop.findOne({shop: shop_id}, function (err, shopdata) {
        Instagram.findOne({shop:shop_id},function (err,instadata) {
            if(!err && instadata!=null){
                if(instadata.page_id!='' && instadata.insta_id!=''){
                    Facebookpage.findOne({shop:shop_id,page_id:instadata.page_id},function (err,pagedata) {
                          if(pagedata!=null){

                              const InstagramDataURL = "https://graph.facebook.com/"+ instadata.insta_id +"?fields=profile_picture_url&access_token="+ pagedata.page_accesss_token;

                              request.get(InstagramDataURL)
                                  .then((Instaprresponse) => {
                                  var profilrdata = JSON.parse(Instaprresponse);

                              // const InstagramURL = "https://graph.facebook.com/"+ instadata.insta_id +"/media?fields=media_url&access_token="+ pagedata.page_accesss_token;
                              const InstagramURL = "https://graph.facebook.com/"+ instadata.insta_id +"/media?fields=media_url,caption,like_count,timestamp,comments_count,text,user,username,permalink&access_token="+ pagedata.page_accesss_token;
                                    console.log(InstagramURL)
                              request.get(InstagramURL)
                                  .then((Instaresponse) => {
                                  // console.log(Instaresponse);

                                  var allpostdata = JSON.parse(Instaresponse);
                                  console.log(allpostdata);

                              var newdataarray = new Array();
                              allpostdata.data.forEach(function (item,value) {

                                  var d = new Date(item.timestamp);
                                  var month = d.getMonth();
                                  var newdate = d.getDate();
                                  const monthNames = ["January", "February", "March", "April", "May", "June",
                                      "July", "August", "September", "October", "November", "December"
                                  ];
                                  var finaldatename = monthNames[month]+' '+ newdate;
                                  var newcaption = '';
                                  if(item.caption){
                                      newcaption = item.caption
                                  }

                                  var newarray = {
                                      'profile' : profilrdata.profile_picture_url,
                                      'text' : item.text,
                                      'media_url' : item.media_url,
                                      'like_count' : item.like_count,
                                      'timestamp' : finaldatename,
                                      'comments': item.comments_count,
                                      'username': item.username,
                                      'caption': newcaption,
                                      'instalink': item.permalink
                                  }
                                  newdataarray.push(newarray);
                              });


                              res.send(newdataarray)

                          }).catch((error1) => {
                                  res.send(false);
                              // console.log(error1);
                          });
                          }).catch((error1) => {
                                  res.send(false);
                              // console.log(error1);
                          });



                          }
                    });


                }

            }

        })
    });

    });


app.post('/get_insta_account', function (req, res) {


    var page_id = req.body.page_id;
    var shop_id = req.body.shop;


    Shop.findOne({shop: shop_id}, function (err, shopdata) {
        if(!err && shopdata!=null){
            Facebookpage.findOne({ page_id : page_id, shop:shop_id },function (err, pagedata) {

                if(!err && pagedata!=null){
                    const accessTokenRequestUrl1 = "https://graph.facebook.com/v6.0/"+ pagedata.page_id +"?fields=instagram_business_account,username&access_token="+ pagedata.page_accesss_token;

                    request.get(accessTokenRequestUrl1)
                        .then((accessTokenResponse) => {
                        console.log(accessTokenResponse);
                   result = JSON.parse(accessTokenResponse);


                    var main_id = pagedata.main_id;
                    var shop = pagedata.shop;
                    var fb_id = pagedata.fb_id;
                    var page_id = pagedata.page_id;
                    var insta_id = result.instagram_business_account.id;

                    if(insta_id!=''){

                            const InstagramDataURL = "https://graph.facebook.com/"+ insta_id +"?fields=name,username,profile_picture_url&access_token="+ pagedata.page_accesss_token;

                            request.get(InstagramDataURL)
                                .then((Instaresponse) => {
                                console.log(Instaresponse);
                                result1 = JSON.parse(Instaresponse);

                             var name = result1.name;
                             var username = result1.username;
                             var profile = result1.profile_picture_url;

                        Instagram.findOne({shop:shop_id },function (err, instadata) {
                            if(!err && instadata==null){
                                const newInstagram = new Instagram({
                                    main_id,
                                    shop,
                                    fb_id,
                                    page_id,
                                    insta_id,
                                    name,
                                    username,
                                    profile
                                });

                                newInstagram.save(function (error) {
                                    console.log("Your Insta has been saved!");
                                    if (error) {
                                        console.error(error);
                                    }
                                });
                            }else{
                                instadata.fb_id = fb_id;
                                instadata.page_id = page_id;
                                instadata.insta_id = insta_id;
                                instadata.name = name;
                                instadata.username = username;
                                instadata.profile = profile;
                                instadata.save();

                            }
                        });

                        res.send(true);

                    }).catch((error1) => {
                                res.send(false);
                            // console.log(error1);
                        });





                    //     const InstagramURL = "https://graph.facebook.com/"+ result.instagram_business_account.id +"/media?fields=media_url&access_token="+ pagedata.page_accesss_token;
                    //
                    //
                    //     request.get(InstagramURL)
                    //         .then((Instaresponse) => {
                    //         console.log(Instaresponse);
                    //     res.send(Instaresponse)
                    //
                    // }).catch((error1) => {
                    //         res.send('false');
                    //     // console.log(error1);
                    // });

                    }else{
                        res.send(false);

                    }

                }).catch((error) => {
                        res.send(false);
                });
                }else{
                    res.send(false);
                }
            });
        }
    });



});




app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});
