const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const facebookpageSchema = new Schema({
    main_id: { type: String, required: true },
    shop: { type: String, required: true },
    fb_id: { type: String, required: true },
    page_id: { type: String, required: true },
    page_name: { type: String},
    page_category: { type: String, required: true },
    page_accesss_token: { type: String, required: true }
}, {
    timestamps: true
});

const Facebookpage = mongoose.model('Facebookpage', facebookpageSchema);

module.exports = Facebookpage;