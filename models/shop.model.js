const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ShopSchema = new Schema({
    shop: { type: String, required: true },
    access_token: { type: String, required: true },
    name: { type: String},
    email: { type: String },
    country: { type: String},
    address1: { type: String},
    created_at: { type: Date},
    country_name: { type: String},
    shop_owner: { type: String},
    customer_email: { type: String},
    plan_name: { type: String},
    cron_status: { type: String},
    delete_status: { type: Number,maxlength: 1}
}, {
    timestamps: true
});

const User = mongoose.model('shop', ShopSchema);

module.exports = User;