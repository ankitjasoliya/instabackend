const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const facebookSchema = new Schema({
    main_id: { type: String, required: true },
    shop: { type: String, required: true },
    name: { type: String, required: true },
    email: { type: String, required: true },
    picture: { type: String},
    userID: { type: String, required: true },
    accesss_token: { type: String, required: true }
}, {
    timestamps: true
});

const Facebook = mongoose.model('Facebook', facebookSchema);

module.exports = Facebook;