const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const settingSchema = new Schema({
    main_id: { type: String, required: true },
    shop: { type: String, required: true },
    info_like : { type: String},
    info_comment: { type: String},
    info_description : { type: String},
    popup_like : { type: String},
    popup_comment : { type: String},
    popup_description : { type: String},
    popover_active : { type: String},
    check_widget : { type: String},
    check_hover : { type: String},
    display_border : { type: String},
    social_sharing : { type: String},
    border_width_value : { type: String},
    widget_heading_value : { type: String},
    layout_column_value : { type: String},
    layout_row_value : { type: String},
    padding_value : { type: String},
    color : { type: String},
    widgetcolor : { type: String},
    overlaycolor : { type: String},
    textcolor : { type: String},
    pbackgroundcolor : { type: String},
    ptextcolor : { type: String},
    shopbackgroundcolor : { type: String},
    shoptextcolor : { type: String},
    shop_heading_value : { type: String},
    shop_button : { type: String}

}, {
    timestamps: true
});

const Settings = mongoose.model('settings', settingSchema);

module.exports = Settings;