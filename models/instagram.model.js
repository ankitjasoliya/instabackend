const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const instagramSchema = new Schema({
    main_id: { type: String, required: true },
    shop: { type: String, required: true },
    fb_id: { type: String, required: true },
    page_id: { type: String, required: true },
    insta_id: { type: String},
    name: { type: String},
    username: { type: String},
    profile: { type: String}
}, {
    timestamps: true
});

const Instagram = mongoose.model('Instagram', instagramSchema);

module.exports = Instagram;